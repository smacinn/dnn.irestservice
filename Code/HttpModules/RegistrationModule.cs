﻿using System;
using System.Web;

using DotNetNuke.ComponentModel;
using DNN.IRestService.Configuration;


namespace DNN.IRestService.HttpModules
{
    public class RegistrationModule : IHttpModule
    {
        /// <summary>
        /// You will need to configure this module in the Web.config file of your
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpModule Members

        public void Dispose()
        {
            //clean-up code here.
        }

        public void Init(HttpApplication context)
        {
            /*  Seems the simple container can only handle () constructors
            // Register DataModel components to the IOC container so that a new instance is returned for each getComponent request
            ComponentFactory.Container.RegisterComponent(typeof(IChannel), typeof(Channel),ComponentLifeStyleType.Transient);
            ComponentFactory.Container.RegisterComponent(typeof(IEdgeServer), typeof(EdgeServer),ComponentLifeStyleType.Transient);
            ComponentFactory.Container.RegisterComponent(typeof(ISession), typeof(Session), ComponentLifeStyleType.Transient);

            // register
            ComponentFactory.Container.RegisterComponent(typeof(DotNetNuke.Entities.Portals.IPortalController), typeof(DotNetNuke.Entities.Portals.PortalController), ComponentLifeStyleType.Transient);
            ComponentFactory.Container.RegisterComponent(typeof(IChannelDal), typeof(ChannelController), ComponentLifeStyleType.Singleton);
             * */

            new BootStrapper().BootstrapStructureMap();
        }

        #endregion

    }
}
