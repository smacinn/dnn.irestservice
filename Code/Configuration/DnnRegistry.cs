﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StructureMap.Configuration.DSL;

using DotNetNuke.Entities.Portals;

using IRestService.Interfaces.DataModel;
using IRestService.Impl.DataModel;
using IRestService.Interfaces.Format;
using IRestService.Impl.Format;
using IRestService.Interfaces.Handler;
using IRestService.Impl.Handler;
using IRestService.Interfaces.Serializer;
//using IRestService.Impl.Serializer;
using IRestService.Interfaces.Service;
using IRestService.Impl.Service;

using DNN.IRestService.Interfaces.DataModel;
using DNN.IRestService.DataModel;
using DNN.IRestService.Handlers;

namespace DNN.IRestService.Configuration
{
    public class DnnRegistry : Registry
    {
        public DnnRegistry()
        {
            For<IPortalController>().Use<PortalController>();

            // DnnUser Active DataModels

            For<IPermission>().Use<Permission>();
            For<IDnnUser>().Use<DnnUser>().Name = "GET";


            // rest handlers for rest services
            For<IRestHandler>().Use<ExceptionHandler>();
            For<IRestHandler>().Add<DnnExceptionHandler>().Name = "exception";
            For<IRestHandler>().Add<PasswordHandler>().Name = "password";
 

            // Formaters for rest services
            For<IFormat>().Use<RestXmlWriter>();
            For<IFormat>().Add<RestXmlWriter>().Name = "xml";
            For<IFormat>().Add<RestJsonWriter>().Name = "json";
 
        }
    }
}