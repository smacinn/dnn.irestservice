﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StructureMap;


namespace DNN.IRestService.Configuration
{
    public class BootStrapper : IBootstrapper
    {


        #region IBootstrapper Members

        public void BootstrapStructureMap()
        {
            
            ObjectFactory.Configure(x =>
            {
                x.AddRegistry<DnnRegistry>();
                //x.AddRegistry<DataModelRegistry>();
                //x.AddRegistry<DalRegistry>();
                //x.AddRegistry<RestRegistry>();
                //x.AddRegistry<DnnRegistry>();
            });
        }

        #endregion
    }
}