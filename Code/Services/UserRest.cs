﻿using System;
using System.Collections.Specialized;
using System.Web;

using StructureMap;
using DotNetNuke.Entities.Portals;

using IRestService.Interfaces.Format;
using IRestService.Interfaces.Handler;
using IRestService.Interfaces.Serializer;
using IRestService.Interfaces.Service;
using IRestService.Interfaces.DataModel;
using IRestService.Impl.Service;
using IRestService.Impl.DataModel;

using DNN.IRestService.Interfaces.DataModel;


namespace DNN.IRestService.Services
{
    /// <summary>
    /// The expected incoming url format is:
    /// format/Handler/Method/Param/One/ParamTwo/Two
    /// IE: xml/Employee/list/lastname/smith
    /// 
    /// To help debug use the following query parameters
    ///  IE: xml/Employee/list/lastname/smith?debug=true
    /// </summary>

    public class UserRest : BaseRestService,  IHttpHandler   
    {
        IDnnUser _user = null;

        public UserRest() : base()
        {
        
        }

        
        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Cache.SetExpires(DateTime.MinValue);
            context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

            // parse the request Details
            base.ParseRequest(context);


            // set the Formator
            try
            {
                // look for a formater that named aftre the component and requested format
                this.Formator = ObjectFactory.GetNamedInstance<IFormat>(this.Request.Component + this.Request.Format);
                //this.Formator = ObjectFactory.GetNamedInstance<IFormat>(this.Format);
            }
            catch (Exception ef1)
            {
                try
                {
                    // try for a default format request type
                    this.Formator = ObjectFactory.GetNamedInstance<IFormat>(this.Request.Format);
                    if (this.Formator != null)
                    {
                        this.Formator.Append(ef1);
                    }
                }
                catch (Exception ef2)
                {

                        // try for a default format request type
                        this.Formator = ObjectFactory.GetInstance<IFormat>();
                        if (this.Formator != null)
                        {
                            this.Formator.Append(ef1);
                            this.Formator.Append(ef2);
                        }
                        this.Formator.Append("Invalid Format Request, defaulting to: " + this.Formator.Name);
                }
            }

            //if (context.Request.HttpMethod == "GET")
            //{
            //    try
            //    {
            //        _user = ObjectFactory.GetInstance<IDnnUser>();
            //    }
            //    catch (Exception eu1)
            //    {
            //        this.Formator.Append(eu1);
            //        _user = null;
            //    }
            //}
            //else if (context.Request.HttpMethod == "POST")
            //{
                try
                {
                    //_user = ObjectFactory.With("Form").EqualTo(Request.Form).GetInstance<IDnnUser>("remoteuser");

                    _user = ObjectFactory.GetNamedInstance<IDnnUser>(context.Request.HttpMethod);
                }
                catch (Exception eu2)
                {
                    this.Formator.Append(eu2);
                    _user = null;
                    this.Formator.StatusCode = (int)System.Net.HttpStatusCode.MethodNotAllowed;
                    // process the Request
                   // base.Process();

                    // write to the stream
                    //base.Write();
                   // return;
                }
            //}
           // else
            //{
            //    this.Formator.StatusCode = (int)System.Net.HttpStatusCode.MethodNotAllowed;
                // process the Request
            //    base.Process();

                // write to the stream
            //    base.Write();
            //    return;
            //}


            // set the serializer
            if (typeof(ISerializer).IsAssignableFrom(this.Formator.GetType()))
            {
                ISerializer custom = (ISerializer)this.Formator;
                if (custom.Serializer == null)
                {
                    try
                    {
                        custom.Serializer = ObjectFactory.GetNamedInstance<ICustomSerializer>(this.Request.Component + this.Request.Format);
                    }
                    catch (Exception es1)
                    {
                        try
                        {
                            custom.Serializer = ObjectFactory.GetNamedInstance<ICustomSerializer>(this.Request.Format);
                            if (custom.Serializer != null)
                            {
                                this.Formator.Append(es1);
                            }
                        }
                        catch (Exception es2)
                        {
                            custom.Serializer = ObjectFactory.GetInstance<ICustomSerializer>();
                            if (custom.Serializer != null)
                            {
                                this.Formator.Append(es1);
                                this.Formator.Append(es2);
                            }
                        }
                    }
                 }
                this.Formator.Append(new DebugMessage("Serializer", custom.Serializer.GetType().ToString()));
            }

            // set the component handler
            try
            {
                this.Handler = ObjectFactory.With<IUser>(_user).GetInstance<IRestHandler>(this.Request.Component);
                //this.Handler = ObjectFactory.GetNamedInstance<IRestHandler>(this.Request.Component).;
            }
            catch (Exception err)
            {
                try
                {
                    if (this.Formator != null)
                    {
                        Formator.Append(err);
                    }
                    this.Handler = ObjectFactory.GetNamedInstance<IRestHandler>(this.Request.Component);

                }
                catch (Exception err2)
                {
                    if (this.Formator != null)
                    {
                        Formator.Append(err2);
                    }
                    this.Handler = ObjectFactory.GetInstance<IRestHandler>();
                }
            }

            if (this.Request.Query["debug"] != null)
            {
                // override the formator to text output
                //this.Formator = ObjectFactory.GetNamedInstance<IFormat>("text");

                if (Convert.ToBoolean(this.Request.Query["debug"]))
                {
                    this.Formator.Debug = true; 
                    //this.Formator.Append(new DebugMessage("ObjectFactory", ObjectFactory.WhatDoIHave()));
                    this.Formator.Append(new DebugMessage("HttpMethod", context.Request.HttpMethod));
                    this.Formator.Append(new DebugMessage("RestService", this.GetType().ToString()));
                    this.Formator.Append(new DebugMessage("Formater", this.Formator.GetType().ToString()));
                    this.Formator.Append(new DebugMessage("Handler", this.Handler.GetType().ToString()));
                    this.Formator.Append(new DebugMessage("Path", this.Request.Path));
                    this.Formator.Append(new DebugMessage("PathInfo", this.Request.PathInfo));
                    this.Formator.Append(new DebugMessage("QueryString", this.Request.QueryString));
                    this.Formator.Append(new DebugMessage("FormString", this.Request.FormString));
                    this.Formator.Append(new DebugMessage("RawUrl", this.Request.RawUrl));
                    this.Formator.Append(new DebugMessage("Format", this.Request.Format));
                    //this.Formator.Append(new DebugMessage("PortalID", PortalController.GetCurrentPortalSettings().PortalId.ToString()));
                    if (_user != null)
                    {
                        this.Formator.Append(new DebugMessage("RequestorID", this._user.UserId.ToString()));
                        this.Formator.Append(new DebugMessage("Requestor", this._user.UserName));
                        this.Formator.Append(new DebugMessage("RequestorContext", this._user.GetType().ToString()));
                    }
                    else
                    {
                        this.Formator.Append(new DebugMessage("RequestorContext", "User is Null"));
                    }

                    this.Formator.Append(new DebugMessage("Component", this.Request.Component));
                    this.Formator.Append(new DebugMessage("Method", this.Request.Method));
                    this.Formator.Append(new DebugMessage("UrlParameters", this.Request.Parameters.Count > 0?"Yes":"No"));

                    foreach (string key in this.Request.Parameters.AllKeys)
                    {
                        this.Formator.Append(new DebugMessage(key, this.Request.Parameters[key]));
                    }
                    this.Formator.Append(new DebugMessage("QueryParameters", this.Request.Query.Count > 0 ? "Yes" : "No"));
                    foreach (string key in this.Request.Query.AllKeys)
                    {
                        this.Formator.Append(new DebugMessage(key, this.Request.Query[key]));
                    }
                    this.Formator.Append(new DebugMessage("FormParameters", this.Request.Form.Count > 0 ? "Yes" : "No"));
                    foreach (string key in this.Request.Form.AllKeys)
                    {
                        this.Formator.Append(new DebugMessage(key, this.Request.Form[key]));
                    }
                }
            }

            // process the Request
            base.Process();
            
            // write to the stream
            base.Write();
        }

        #endregion

        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return false; }
        }

        #endregion
    }
}