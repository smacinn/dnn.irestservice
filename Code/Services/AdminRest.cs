﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StructureMap;

using IRestService.Interfaces.Format;
using IRestService.Interfaces.Serializer;
using IRestService.Interfaces.Handler;
using IRestService.Interfaces.Service;
using IRestService.Impl.Service;
using IRestService.Impl.DataModel;

namespace DNN.IRestService.Services
{
    public class AdminRest : BaseRestService,  IHttpHandler   
    {
        public AdminRest() : base()
        {
        
        }

        
        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Cache.SetExpires(DateTime.MinValue);

            // parse the request Details
            base.ParseRequest(context);

            // set the Formator
            try
            {
                // look for a formater that named aftre the component and requested format
                this.Formator = ObjectFactory.GetNamedInstance<IFormat>(this.Request.Component+this.Request.Format+"admin");
                //this.Formator = ObjectFactory.GetNamedInstance<IFormat>(this.Format);
            }
            catch(Exception ef1)
            {
                try
                {
                     // try for a default format request type
                    this.Formator = ObjectFactory.GetNamedInstance<IFormat>(this.Request.Component + this.Request.Format);
                    if (this.Formator != null)
                    {
                        this.Formator.Append(ef1);
                    }
                }
                catch (Exception ef2)
                {

                    try
                    {
                         // try for a default format request type
                        this.Formator = ObjectFactory.GetNamedInstance<IFormat>(this.Request.Format);
                        if (this.Formator != null)
                        {
                            this.Formator.Append(ef1);
                            this.Formator.Append(ef2);
                        }
                    }
                    catch (Exception ef3)
                    {

                        this.Formator = ObjectFactory.GetInstance<IFormat>();
                        if (this.Formator != null)
                        {
                            this.Formator.Append(ef1);
                            this.Formator.Append(ef2);
                            this.Formator.Append(ef3);
                        }
                        this.Formator.Append("Invalid Format Request, defaulting to: " + this.Formator.Name);
                     }
                }
            }

            // set the serializer
            if (typeof(ISerializer).IsAssignableFrom(this.Formator.GetType()))
            {
                ISerializer custom = (ISerializer)this.Formator;
                try
                {
                    custom.Serializer = ObjectFactory.GetNamedInstance<ICustomSerializer>(this.Request.Component + this.Request.Format + "admin");
                }
                catch (Exception es1)
                {
                    try
                    {
                        custom.Serializer = ObjectFactory.GetNamedInstance<ICustomSerializer>(this.Request.Component + this.Request.Format);
                        if (custom.Serializer != null)
                        {
                            this.Formator.Append(es1);
                        }
                    }
                    catch (Exception es2)
                    {
                        custom.Serializer = ObjectFactory.GetNamedInstance<ICustomSerializer>(this.Request.Component);
                        if (custom.Serializer != null)
                        {
                            this.Formator.Append(es1);
                            this.Formator.Append(es2);
                        }
                    }
                }
                this.Formator.Append(new DebugMessage("Serializer", custom.Serializer.GetType().ToString()));
            }
 
            // set the component handler
            try
            {
                this.Handler = ObjectFactory.GetNamedInstance<IRestHandler>(this.Request.Component + "admin");

            }
            catch(Exception err)
            {
                try
                {
                    if (this.Formator != null)
                    {
                        Formator.Append(err);
                    }
                    this.Handler = ObjectFactory.GetNamedInstance<IRestHandler>(this.Request.Component);

                }
                catch (Exception err2)
                {
                    if (this.Formator != null)
                    {
                        Formator.Append(err2);
                    }
                    this.Handler = ObjectFactory.GetNamedInstance<IRestHandler>("exception");
                }
            }

            if (this.Request.Query["debug"] != null)
            {
                // override the formator to text output
                //this.Formator = ObjectFactory.GetNamedInstance<IFormat>("text");

                if (Convert.ToBoolean(this.Request.Query["debug"]))
                {
                    this.Formator.Debug = true;
                    this.Formator.Append(new DebugMessage("RestService", this.GetType().ToString()));
                    this.Formator.Append(new DebugMessage("Formater", this.Formator.GetType().ToString()));
                    this.Formator.Append(new DebugMessage("Handler", this.Handler.GetType().ToString()));
                    this.Formator.Append(new DebugMessage("Path", this.Request.Path));
                    this.Formator.Append(new DebugMessage("PathInfo", this.Request.PathInfo));
                    this.Formator.Append(new DebugMessage("QueryString", this.Request.QueryString));
                    this.Formator.Append(new DebugMessage("RawUrl", this.Request.RawUrl));
                    this.Formator.Append(new DebugMessage("Format", this.Request.Format));

                    this.Formator.Append(new DebugMessage("Component", this.Request.Component));
                    this.Formator.Append(new DebugMessage("Method", this.Request.Method));
                    this.Formator.Append(new DebugMessage("Parameters", ":"));
                    
                    foreach (string key in this.Request.Parameters.AllKeys)
                    {
                        this.Formator.Append(new DebugMessage(key, this.Request.Parameters[key]));
                    }
                    this.Formator.Append(new DebugMessage("Query", ":"));
                    foreach (string key in this.Request.Query.AllKeys)
                    {
                        this.Formator.Append(new DebugMessage(key, this.Request.Query[key]));
                    }
                }
            }
            // process the Request
            base.Process();
            // write to the stream
            base.Write();
        }

        #endregion

        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return false; }
        }

        #endregion
    }
}