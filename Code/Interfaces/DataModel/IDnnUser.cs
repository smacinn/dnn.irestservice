﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IRestService.Interfaces.DataModel;

namespace DNN.IRestService.Interfaces.DataModel
{
    public interface IDnnUser: IUser
    {
        int PortalId { get; set; }

    }
}
