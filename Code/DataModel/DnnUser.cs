﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using StructureMap;

using IRestService.Interfaces.DataModel;
using IRestService.Impl.Exceptions;
using DNN.IRestService.Interfaces.DataModel;

using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Roles;
using DotNetNuke.Entities.Portals;

namespace DNN.IRestService.DataModel
{
    public class DnnUser : IDnnUser
    {
        UserInfo _user = null;
        List<IPermission> _permissons = null;

        public DnnUser()
        {
            //_user = UserController.GetUserByName(PortalController.GetCurrentPortalSettings().PortalId, System.Web.HttpContext.Current.User.Identity.Name);
            _user = UserController.GetUserByName(0, System.Web.HttpContext.Current.User.Identity.Name);
            if (_user != null)
            {
                _permissons = new List<IPermission>();
                GetPermissions();
            }
        }

        public int PortalId
        {
            get
            {
                    return PortalController.GetCurrentPortalSettings().PortalId;
            }
            set
            {

            }
        }

        public bool IsAdministrator
        {
            get
            {
                return (_user.IsSuperUser || _user.IsInRole("administrators"));
            }
            set
            {

            }
        }

        public bool IsAuthorized
        {
            get
            {
                if (_user.Membership != null)
                {
                    return (_user.Membership.Approved && !_user.Membership.LockedOut);
                }
                else
                {
                    return false;
                }
            }
            set
            {

            }
        }

        public int UserId
        {
            get
            {
                if (_user != null)
                {
                    return _user.UserID;
                }
                else
                {
                    return -1;
                }
            }
            set
            {

            }
        }

        public string Email
        {
            get
            {
                if (_user != null)
                {
                    return _user.Email;
                }
                else
                {
                    return string.Empty;
                }

            }
            set
            {

            }
        }

        public string UserName
        {
            get
            {
                if (_user != null)
                {
                    return _user.Username;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {

            }
        }

        public string FirstName
        {
            get
            {
                if (_user != null)
                {
                    return _user.FirstName;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {

            }
        }

        public string LastName
        {
            get
            {
                if (_user != null)
                {
                    return _user.LastName;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {

            }
        }

        public string Address
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Region
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string RegionCode
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Country
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Phone
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Cell
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public List<IPermission> Permissons
        {
            get
            {
                return _permissons;
            }
            set
            {

            }
        }

        protected void GetPermissions()
        {
            IList<UserRoleInfo> roles = null;
            RoleController ctrl = new RoleController();
            roles = ctrl.GetUserRoles(_user, true);

            foreach (UserRoleInfo role in roles)
            {
                IPermission p = ObjectFactory.GetInstance<IPermission>();
                p.PermissionId = role.RoleID;
                p.Title = role.RoleName;

                _permissons.Add(p);
            }

        }

    }
}
