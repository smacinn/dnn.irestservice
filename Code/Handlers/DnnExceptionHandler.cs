﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

using IRestService.Interfaces.DataModel;
using IRestService.Interfaces.Handler;
using IRestService.Impl.Exceptions;



namespace DNN.IRestService.Handlers
{
    public class DnnExceptionHandler : IRestHandler
    {
        protected int _statusCode = (int)System.Net.HttpStatusCode.MethodNotAllowed;
        public int StatusCode
        {
            get
            {
                return _statusCode;
            }
        }

        IUser _user = null;
        public DnnExceptionHandler(IUser User)
        {
            _user = User;
        }
        public object Process(string method, NameValueCollection parameters)
        {
            return new RestException();
        }
    }
}
